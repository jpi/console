package resource

import (
	"go.jpi.io/console/resource/pathparser"
	"net/http"
	"regexp"
	"strings"
)

func (o *Resource) getDispatcher(r *http.Request) (http.HandlerFunc, bool) {
	// at this point the environment always exists
	env, _ := o.Environment(r)
	path := env["PATH_INFO"]

	for p, h := range o.patterns {
		if m := p.FindStringSubmatch(path); m != nil {
			complete := true

			for i, k := range p.SubexpNames() {
				s := m[i]

				if s == "" {
					complete = false
				} else if k != "" {
					env[k] = s
				}
			}

			s := m[0]
			l := len(s)

			if strings.HasSuffix(s, "/") {
				l--
				s = s[:l]
			}
			env["SCRIPT_NAME"] += s
			env["PATH_INFO"] = path[l:]

			return h, complete
		}
	}
	return nil, false
}

func (o *Resource) HandleFunc(path string, f http.HandlerFunc) {
	p, _ := pathparser.MustCompile(path)

	if o.patterns == nil {
		o.patterns = make(map[*regexp.Regexp]http.HandlerFunc)
	}

	o.patterns[p] = f
}

func (o *Resource) Handle(path string, h http.Handler) {
	o.HandleFunc(path, h.ServeHTTP)
}

func (o *Resource) SetFallback(h http.HandlerFunc) {
	o.fallback = h
}
