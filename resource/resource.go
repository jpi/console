package resource // import "go.jpi.io/console/resource"

import (
	"net/http"
	"regexp"
	"strings"
)

type Handler interface {
	http.Handler
}

type Resource struct {
	methods  map[string]http.HandlerFunc
	patterns map[*regexp.Regexp]http.HandlerFunc
	fallback http.HandlerFunc
	allow    string
}

// Errors
func (o *Resource) RaiseNotFound(w http.ResponseWriter, _ *http.Request) {
	http.Error(w, "Resource not found", http.StatusNotFound)
}

// http.Handler
func (o *Resource) ServeHTTP(p Handler, w http.ResponseWriter, r *http.Request) {
	if o.methods == nil {
		// Initialise
		o.methods = make(map[string]http.HandlerFunc, 5)
		allow, i := make([]string, 4), 0

		if f, ok := p.(Getter); ok {
			o.methods["HEAD"] = f.Get
			o.methods["GET"] = f.Get
			allow[i] = "GET"
			i++
		}

		if f, ok := p.(Poster); ok {
			o.methods["POST"] = f.Post
			allow[i] = "POST"
			i++
		}

		if f, ok := p.(Putter); ok {
			o.methods["PUT"] = f.Put
			allow[i] = "PUT"
			i++
		}

		if f, ok := p.(Deleter); ok {
			o.methods["DELETE"] = f.Delete
			allow[i] = "DELETE"
			i++
		}

		o.allow = strings.Join(allow[:i], ",")
	}

	if len(o.patterns) > 0 || o.fallback != nil {
		// Includes internal dispatcher, prepare environment
		env, r := o.Environment(r)

		if f, complete := o.getDispatcher(r); f != nil {
			if complete || strings.HasSuffix(r.URL.Path, "/") {
				f(w, r)
			} else {
				http.Redirect(w, r, r.URL.Path+"/", http.StatusMovedPermanently)
			}

			return
		} else if env["PATH_INFO"] != "/" {

			if o.fallback != nil {
				o.fallback(w, r)
			} else {
				http.NotFound(w, r)
			}

			return
		}
	}

	if f, ok := o.methods[r.Method]; ok {
		f(w, r)
	} else {
		// Method Not Allowed
		o.NotAllowed(w, r, o.allow)
	}
}
