package resource

import (
	"fmt"
	"net/http"
)

func (_ *Resource) BadRequest(w http.ResponseWriter, s string, args ...interface{}) {
	if len(args) > 0 {
		s = fmt.Sprintf(s, args...)
	}
	http.Error(w, s, http.StatusBadRequest)
}
