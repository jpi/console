package resource

import (
	"net/http"
)

// GET {resource}
type Getter interface {
	Get(w http.ResponseWriter, r *http.Request)
}

// POST {resource}
type Poster interface {
	Post(w http.ResponseWriter, r *http.Request)
}

// PUT {resource}
type Putter interface {
	Put(w http.ResponseWriter, r *http.Request)
}

// DELETE {resource}
type Deleter interface {
	Delete(w http.ResponseWriter, r *http.Request)
}
