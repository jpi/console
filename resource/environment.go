package resource

import (
	"context"
	"fmt"
	"net/http"
	"strconv"
	"strings"
)

const DispatcherEnv = "X-Dispatcher-Env"

// environment table
type Environment map[string]string

func (env Environment) Set(key, value string) {
	env[key] = value
}

func (env Environment) SetInt(key string, value int) {
	env.Set(key, fmt.Sprintf("%v", value))
}

func (env Environment) Get(key string) string {
	if v, ok := env[key]; ok && len(v) > 0 {
		return strings.TrimSpace(v)
	}
	return ""
}

func (env Environment) GetInt64(key string, base int) (int64, error) {
	s := env.Get(key)
	return strconv.ParseInt(s, base, 64)
}

func (env Environment) GetInt(key string, base int) (int, error) {
	v, err := env.GetInt64(key, base)
	if err != nil {
		return 0, err
	}

	return int(v), nil
}

func GetEnvironment(r *http.Request) (Environment, *http.Request) {
	ctx := r.Context()
	env, ok := ctx.Value(DispatcherEnv).(Environment)
	if !ok {
		env = make(Environment)
		env["SCRIPT_NAME"] = ""
		env["PATH_INFO"] = r.URL.Path

		ctx = context.WithValue(ctx, DispatcherEnv, env)
		r = r.WithContext(ctx)
	}
	return env, r
}

// static convenience methods
func (_ *Resource) Environment(r *http.Request) (map[string]string, *http.Request) {
	return GetEnvironment(r)
}

func (_ *Resource) EnvValue(r *http.Request, key string) string {
	env, _ := GetEnvironment(r)
	return env.Get(key)
}

func (_ *Resource) EnvValueInt(r *http.Request, key string) (int, error) {
	env, _ := GetEnvironment(r)
	return env.GetInt(key, 0)
}
