package resource // import "go.jpi.io/console/resource"

import (
	"fmt"
	"net/http"
	"strconv"
	"strings"
)

// public http.Request.Form helpers
func parseForm(r *http.Request) error {
	var err error

	t := strings.Split(r.Header.Get("Content-Type"), ";")[0]
	if t == "application/x-www-form-urlencoded" {
		err = r.ParseForm()
	} else if strings.HasPrefix(t, "multipart/form-data") {
		err = r.ParseMultipartForm(1 << 20)
	} else {
		err = fmt.Errorf("Invalid Content-Type %q", t)
	}

	return err
}

func FormValue(r *http.Request, key string) (string, error) {
	if r.Form == nil {
		if err := parseForm(r); err != nil {
			return "", err
		}
	}

	if v, ok := r.Form[key]; ok {
		return strings.TrimSpace(v[0]), nil
	} else {
		return "", nil
	}
}

func formValueInt(r *http.Request, key string, base int, bitSize int) (int64, error) {
	s, err := FormValue(r, key)
	if err != nil {
		return 0, err
	} else {
		return strconv.ParseInt(s, base, bitSize)
	}
}

func FormValueInt32(r *http.Request, key string, base int) (int32, error) {
	v, err := formValueInt(r, key, base, 32)
	return int32(v), err
}

func FormValueInt16(r *http.Request, key string, base int) (int16, error) {
	v, err := formValueInt(r, key, base, 16)
	return int16(v), err
}

func formValueFloat(r *http.Request, key string, bitSize int) (float64, error) {
	s, err := FormValue(r, key)
	if err != nil {
		return 0, err
	} else {
		return strconv.ParseFloat(s, bitSize)
	}
}

func FormValueFloat32(r *http.Request, key string) (float32, error) {
	v, err := formValueFloat(r, key, 32)
	return float32(v), err
}

func FormValueBool(r *http.Request, key string) (bool, error) {
	s, err := FormValue(r, key)
	if err != nil {
		return false, err
	} else {
		return strconv.ParseBool(s)
	}
}

// and as static methods
func (_ *Resource) ParseForm(r *http.Request) error {
	var err error
	if r.Form == nil {
		err = parseForm(r)
	}

	return err
}

func (o *Resource) FormValue(r *http.Request, key string) (string, error) {
	return FormValue(r, key)
}

func (o *Resource) FormValueInt32(r *http.Request, key string, base int) (int32, error) {
	return FormValueInt32(r, key, base)
}

func (o *Resource) FormValueFloat32(r *http.Request, key string) (float32, error) {
	return FormValueFloat32(r, key)
}

func (o *Resource) FormValueBool(r *http.Request, key string) (bool, error) {
	return FormValueBool(r, key)
}
