package resource

import (
	"fmt"
	"go.jpi.io/console/resource/mimeparse"
	"net/http"
)

func (_ Resource) BestMime(r *http.Request, supported ...string) (string, error) {
	if s := mimeparse.BestMatch(supported, r.Header.Get("Accept")); len(s) > 0 {
		return s, nil
	}

	return "", fmt.Errorf("Supported types %q", supported)
}
