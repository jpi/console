package resource

import (
	"fmt"
	"net/http"
)

func (o *Resource) NotFound(w http.ResponseWriter, r *http.Request) {
	http.NotFound(w, r)
}

func (o *Resource) Allowed(r *http.Request, method string) bool {
	if r.Method == "HEAD" && method == "GET" {
		return true
	} else if r.Method == method {
		return true
	} else {
		return false
	}
}

func (o *Resource) NotAllowed(w http.ResponseWriter, r *http.Request, allow string) {
	w.Header().Add("Allow", allow)
	http.Error(w, fmt.Sprintf("Method %s Not Allowed", r.Method), http.StatusMethodNotAllowed)
}
