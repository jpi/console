package main

import (
	"fmt"
	"os"
	"text/template"
)

type file struct {
	Filename, Variable string
}

type data struct {
	Type    string
	Entries []file
}

func (c config) renderInit(fout *os.File, names, vars []string) error {
	d := data{
		Entries: make([]file, len(vars)),
	}

	if c.template {
		d.Type = "static.Blob"
	} else {
		d.Type = "static.Content"
	}
	for i, fname := range names {
		d.Entries[i] = file{
			Filename: fmt.Sprintf("/%s", fname),
			Variable: vars[i],
		}
	}

	t := template.Must(template.New("Init").Parse(`
var Files map[string]{{.Type}}

func init() {
	Files = make(map[string]{{.Type}}, {{len .Entries}})
{{range .Entries}}
	Files[{{printf "%q" .Filename}}] = {{.Variable}}{{end}}
}
`))
	return t.Execute(fout, d)
}
